using UnityEngine;

public class Patrolling : MonoBehaviour
{
    [SerializeField] private Transform[] _pathPoints;
    [SerializeField] private int _speedMoving;

    private int _currentPoint;
    private float _distanceBetweenPoints;
    private const float _distancePointRange = 1f;

    private void Start()
    {
        SetFirstPoint();
        LookNextPosition();
    }

    private void Update()
    {
        DistancePoints();
        CheckingDistanceRange();
        MovingPatrol();
    }   

    private void SetFirstPoint()
    {
        _currentPoint = 0;
    }

    private void LookNextPosition()
    {
        transform.LookAt(_pathPoints[_currentPoint].position);
    }

    private void DistancePoints()
    {
        _distanceBetweenPoints = Vector3.Distance(transform.position,
            _pathPoints[_currentPoint].position);
    }

    private void CheckingDistanceRange()
    {
        if (_distanceBetweenPoints < _distancePointRange)
            NextPoint();
    }

    private void MovingPatrol()
    {
        transform.Translate(Vector3.forward * _speedMoving * Time.deltaTime);
    }

    private void NextPoint()
    {
        _currentPoint++;

        if (_currentPoint >= _pathPoints.Length)
            _currentPoint = 0;

        LookNextPosition();
    }
}
