using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Enemy[] _enemies;
    [SerializeField] private GameObject _containerEnemies;
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private Vector3 _sidesSquare;
    [SerializeField] private float _secondsBetweenSpawn;

    private IEnumerator SpawnRandomEnemy()
    {
        while (true)
        {
            Enemy newEnemy = Instantiate(_enemies[Random.Range(0, _enemies.Length)],
              GetRandomPlaceInFlatSquare(), Quaternion.identity, _containerEnemies.transform);          

            yield return new WaitForSeconds(_secondsBetweenSpawn);
        }
    }

    private Vector3 GetRandomPlaceInFlatSquare()
    {
        Vector3 position = new Vector3(GetRandomSidesX(_spawnPoint, _sidesSquare),
            GetSidesY(_spawnPoint), GetRandomSidesZ(_spawnPoint, _sidesSquare));

        return position;
    }

    private float GetRandomSidesX(Transform spawnPoint, Vector3 sidesSquare)
    {
        return Random.Range(spawnPoint.position.x - sidesSquare.x, 
            spawnPoint.position.x + sidesSquare.x);
    }

    private float GetSidesY(Transform spawnPoint)
    {
        return spawnPoint.position.y;
    }

    private float GetRandomSidesZ(Transform spawnPoint, Vector3 sidesSquare)
    {
        return Random.Range(spawnPoint.position.z - sidesSquare.z, 
            spawnPoint.position.z + sidesSquare.z);
    }

}