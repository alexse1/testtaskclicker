using UnityEngine;
using UnityEngine.EventSystems;

public class ClickKiller: MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, 
            Input.mousePosition.y, 1));
        
        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
        {
            if (hit.transform.TryGetComponent(out Enemy enemy))
            {
                enemy.Die();
                Destroy(hit.transform.gameObject);
            }
        }
    }

}
