using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("StartScreen")]
    [SerializeField] private PlayButton _playButton;
    [SerializeField] private ExitButton _exitButton;

    [Header("GameOverScreen")]
    [SerializeField] private RestartButton _restartButton;
    
    [Space(5)]
    [SerializeField] private GameObject _zoneClickKiller;
    [SerializeField] private Spawner _spawnerEnemy;

    private Enemy[] _enemies => FindObjectsOfType<Enemy>();

    private const int _allowedCountMonsters = 10;

    public const string SpawnRandomEnemy = "SpawnRandomEnemy";

    private void OnEnable()
    {
        _playButton.PlayButtonClick += OnPlayButtonClick;
        _exitButton.ExitButtonClick += OnExitButtonClick;

        _restartButton.RestartButtonClick += OnRestartButtonClick;
    }

    private void OnDisable()
    {
        _playButton.PlayButtonClick -= OnPlayButtonClick;
        _exitButton.ExitButtonClick -= OnExitButtonClick;

        _restartButton.RestartButtonClick -= OnRestartButtonClick;
    }

    private void Start()
    {
        StartGame();
    }

    private void Update()
    {
        DefeatConditions();
    }

    private void OnPlayButtonClick()
    {
        _playButton.CloseScreen();
        _zoneClickKiller.SetActive(true);

        StartGameWithButton();
    }

    private void OnRestartButtonClick()
    {
        _restartButton.CloseScreen();
        _zoneClickKiller.SetActive(true);

        StartGameWithButton();
    }

    private void OnExitButtonClick()
    {
        Application.Quit();
    }

    private void StartGame()
    {
        Time.timeScale = 0;
        _playButton.OpenScreen();
    }

    private void DefeatConditions()
    {
        if (_enemies.Length >= _allowedCountMonsters)
        {
            GameOver();
        }
    }

    private void StartGameWithButton()
    {
        _spawnerEnemy.StartCoroutine(nameof(SpawnRandomEnemy));
        Time.timeScale = 1;
    }

    private void GameOver()
    {
        _spawnerEnemy.StopCoroutine(nameof(SpawnRandomEnemy));
        _zoneClickKiller.SetActive(false);
        _restartButton.OpenScreen();

        foreach (var enemies in _enemies)
        {
            Destroy(enemies.gameObject);
        }

    }

}
