using UnityEngine.Events;

public class PlayButton : Screen
{
    public event UnityAction PlayButtonClick;

    public override void CloseScreen()
    {
        PanelMenu.SetActive(false);
    }

    public override void OpenScreen()
    {
        PanelMenu.SetActive(true);
    }

    protected override void OnButtonClick()
    {
        PlayButtonClick?.Invoke();
    }
}
