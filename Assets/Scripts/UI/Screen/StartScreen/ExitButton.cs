using UnityEngine.Events;

public class ExitButton : Screen
{
    public event UnityAction ExitButtonClick;

    protected override void OnButtonClick()
    {
        ExitButtonClick?.Invoke();
    }
}
