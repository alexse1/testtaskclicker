using UnityEngine.Events;

public class RestartButton : Screen
{
    public event UnityAction RestartButtonClick;

    public override void CloseScreen()
    {
        PanelMenu.SetActive(false);
    }

    public override void OpenScreen()
    {
        PanelMenu.SetActive(true);
    }

    protected override void OnButtonClick()
    {
        RestartButtonClick?.Invoke();
    }
}
